# Package

version       = "0.0.1"
author        = "Loïc Bartoletti"
description   = "A fixed point number library in pure Nim."
license       = "MIT"
srcDir        = "src"

# Dependencies

requires "nim >= 1.0.2"

task docs, "Build documentation":
  exec "nim doc --index:on --outdir:pages --project src/fpn.nim"


task test, "Runs all tests":
  exec "nim c -r tests/init.nim"
  exec "nim c -r tests/addition.nim"
  exec "nim c -r tests/substraction.nim"
  exec "nim c -r tests/multiplication.nim"
  exec "nim c -r tests/division.nim"
  exec "nim c -r tests/abs.nim"
  exec "nim c -r tests/ceil.nim"
  exec "nim c -r tests/floor.nim"
  exec "nim c -r tests/round.nim"
  exec "nim c -r tests/whole_float_parts.nim"
  exec "nim c -r tests/square_root.nim"

task test_init, "init tests":
  exec "nim c -r tests/init.nim"

task test_addition, "addition tests":
  exec "nim c -r tests/addition.nim"

task test_substraction, "substraction tests":
  exec "nim c -r tests/substraction.nim"

task test_multiplication, "multiplication tests":
  exec "nim c -r tests/multiplication.nim"

task test_division, "division tests":
  exec "nim c -r tests/division.nim"

task test_abs, "abs tests":
  exec "nim c -r tests/abs.nim"

task test_ceil, "ceil tests":
  exec "nim c -r tests/ceil.nim"

task test_floor, "floor tests":
  exec "nim c -r tests/floor.nim"

task test_round, "round tests":
  exec "nim c -r tests/round.nim"

task test_whole_float_parts, "whole/float parts tests":
  exec "nim c -r tests/whole_float_parts.nim"

task test_square_root, "square root tests":
  exec "nim c -r tests/square_root.nim"
