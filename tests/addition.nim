import unittest

import fpn

test "[over|under]flow_add":
  var a = initQ16_16()
  var b = initQ16_16()

  check not isOverflowAdd(a,b)
  check not isUnderflowAdd(a,b)
  check isSafeAdd(a,b)

  a.fromInt(high(int16))
  b.fromInt(1)
  check isOverflowAdd(a,b)
  check not isUnderflowAdd(a,b)
  check isSafeAdd(a,b) == false
  check toInt(a.ovAdd(b)) == low(int16)

  a.fromInt(low(int16))
  b.fromInt(-1)
  check isUnderflowAdd(a,b)
  check not isOverflowAdd(a,b)
  check isSafeAdd(a,b) == false
  check toInt(a.ovAdd(b)) == high(int16)

  a.fromInt(low(int16)+1)
  b.fromInt(-1)
  check not isUnderflowAdd(a,b)
  check not isOverflowAdd(a,b)
  check isSafeAdd(a,b)
  check toInt(a.ovAdd(b)) == low(int16)

  a.fromInt(high(int16)-1)
  b.fromInt(1)
  check not isOverflowAdd(a,b)
  check not isUnderflowAdd(a,b)
  check isSafeAdd(a,b)
  check toInt(a.ovAdd(b)) == high(int16)

test "addition":
  var a = initQ16_16()
  var b = initQ16_16()
  check add(a, b).rawData() == 0
  check rawData(a + b) == 0
  a += b
  check a.rawData() == 0

  a.fromFloat(40.4)
  b.fromFloat(1.6)
  check add(a, b).toInt() == 42
  check add(a, b).toFloat() == 42.0

test "saturated addition":
  var a = initQ16_16()
  var b = initQ16_16()
  a.fromInt(high(int16))
  b.fromInt(1)
  check satAdd(a, b).toInt() == high(int16)

  a.fromInt(low(int16))
  b.fromInt(-1)
  check satAdd(a, b).toInt() == low(int16)
