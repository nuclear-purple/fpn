import unittest
import math

import fpn

proc myRound*[T: float32|float64](x: T, places: int): T=
  if places == 0:
    result = round(x)
  else:
    var mult = pow(10.0, places.T)
    result = round(x*mult)/mult

test "init fpn":
  var q = initQ16_16()
  check q.rawData() == 0

  check toInt(q) == 0

  q.fromInt(0)
  check q.rawData() == 0
  check toInt(q) == 0

test "init with min/max int":
  var q = initQ16_16()

  q.fromInt(high(int16))
  check q.rawData() == 2147418112
  check toInt(q) == high(int16)

  q.fromInt(low(int16))
  check q.rawData() == -2147483648
  check toInt(q) == low(int16)

test "init with some int values":
  var q = initQ16_16()

  q.fromInt(1)
  check q.rawData() == 65536
  check toInt(q) == 1

  q.fromInt(-1)
  check q.rawData() == -65536
  check toInt(q) == -1

  q.fromInt(12345)
  check q.rawData() == 809041920
  check toInt(q) == 12345

test "init from float":
  var q = initQ16_16()

  q.fromFloat(0.0)
  check q.rawData() == 0
  check toFloat(q) == 0

  let pi = 3.14159265358979311599796346854419
  q.fromFloat(pi)
  check q.rawData() == 205887
  check $myRound(toFloat(q), 16) == $myRound(3.141586303710938, 16)

  let e = 2.71828182845904509079559829842765
  q.fromFloat(e)
  check q.rawData() == 178145
  check $myRound(toFloat(q), 16) == $myRound(2.7182769775390625, 16)

  let l10 = 2.30258509299404590109361379290931
  q.fromFloat(l10)
  check q.rawData() == 150902
  check $myRound(toFloat(q), 16) == $myRound(2.3025817871093750, 16)

