import unittest

import fpn

test "square root":
  var a = initQ16_16()
  var res = initQ16_16()

  check isqrt(a) == a

  a.fromInt(-4)
  check isqrt(a).rawData() == low(int32)

  a.fromInt(4)
  res.fromInt(2)
  check isqrt(a) == res

  a.fromInt(9)
  res.fromInt(3)
  check isqrt(a) == res

  a.fromInt(1234)
  res.fromInt(35)
  check isqrt(a) == res
