# test64.nim

import unittest
import fpn
import std/monotimes
import times
import strutils
import math

genQM_N(fix32, 32, fixedPoint64)
genQM_N(fix16, 16, fixedPoint32)

test "multiplication":
  # check if it works for 64 bit fixed point numbers
  let a = fix32(30)
  let b = fix32(20)
  check a * b == fix32(600)

  # check the accuracy compared to the old proc
  var fp16 = fix16()
  let c = fp16.pi
  let d = fp16.e
  check mul64(c, d) == ovMul(c, d)

  # test for negative numbers
  let x = fix32(-30)
  let y = fix32(20)
  check x * y == fix32(-600)
  check x * x == fix32(900)

test "benchmark mul":
  var a = fix16(30)
  var b = fix16(2)
  var c = fix16(1, 1)

  let time1 = getMonoTime()
  for i in countup(1, 100000):
    a = mul64(a, b)
    a = mul64(a, c)
  let time2 = getMonoTime()
  let timeNew = time2 - time1

  let time3 = getMonoTime()
  for i in countup(1, 100000):
    a = ovMul(a, b)
    a = ovMul(a, c)
  let time4 = getMonoTime()
  let timeOld = time4 - time3

  var x = 30.0
  var y = 2.0
  var z = 0.5
  let time5 = getMonoTime()
  for i in countup(1, 100000):
    x = x * y
    x = x * z
  let time6 = getMonoTime()
  let timeFloat = time6 - time5

  echo "new ovMul: ", timeNew
  echo "old ovMul: ", timeOld
  echo "float mul: ", timeFloat

# Division tests
test "division":
  # check if it works for 64 bit fixed point numbers
  let a = fix32(30)
  let b = fix32(20)
  check div64(a, b) == fix32(0b11, 1)

  # check the accuracy compared to the old proc
  var fp16 = fix16()
  let c = fp16.pi
  let d = fp16.e
  check ovDiv(c, d) == div64(c, d)

  # test for negative numbers
  let x = fix32(-30)
  let y = fix32(20)
  check div64(x, y) == -fix32(3, 1)

  # test operators
  let i = 30
  check x / i == fix32(-1)

test "benchmark div":
  var a = fix16(30)
  var b = fix16(2)
  var c = fix16(1, 1)

  let time1 = getMonoTime()
  for i in countup(1, 100000):
    a = ovDiv(a, b)
    a = ovDiv(a, c)
  let time2 = getMonoTime()
  let timeOld = time2 - time1

  let time3 = getMonoTime()
  for i in countup(1, 100000):
    a = div64(a, b)
    a = div64(a, c)
  let time4 = getMonoTime()
  let timeNew = time4 - time3

  let time5 = getMonoTime()
  for i in countup(1, 100000):
    a = div32(a, b)
    a = div32(a, c)
  let time6 = getMonoTime()
  let timeNew32 = time6 - time5

  echo timeNew
  echo timeNew32
  echo timeOld

test "negative":
  # echo int32(-1.0 * pow(float32(2), 16)).toBin(32)
  # echo int32(-1.5 * pow(float32(2), 16)).toBin(32)

  let a = fix16(-0b101, 2)
  check $a == "-1.25"
  echo a.toInt
