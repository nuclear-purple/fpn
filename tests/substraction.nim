import unittest

import fpn

test "[over|under]flow_sub":
  var a = initQ16_16()
  var b = initQ16_16()

  check not isOverflowSub(a,b)
  check not isUnderflowSub(a,b)
  check isSafeSub(a,b)

  a.fromInt(high(int16))
  b.fromInt(-1)
  check isOverflowSub(a,b)
  check not isUnderflowSub(a,b)
  check isSafeSub(a,b) == false
  check toInt(a.ovSub(b)) == low(int16)

  a.fromInt(low(int16))
  b.fromInt(1)
  check isUnderflowSub(a,b)
  check not isOverflowSub(a,b)
  check isSafeSub(a,b) == false
  check toInt(a.ovSub(b)) == high(int16)

  a.fromInt(low(int16)+1)
  b.fromInt(1)
  check not isUnderflowSub(a,b)
  check not isOverflowSub(a,b)
  check isSafeSub(a,b)
  check toInt(a.ovSub(b)) == low(int16)

  a.fromInt(high(int16)-1)
  b.fromInt(-1)
  check not isOverflowSub(a,b)
  check not isUnderflowSub(a,b)
  check isSafeSub(a,b)
  check toInt(a.ovSub(b)) == high(int16)

test "substraction":
  var a = initQ16_16()
  var b = initQ16_16()
  check sub(a, b).rawData() == 0
  check rawData(a + b) == 0
  a -= b
  check a.rawData() == 0

  a.fromFloat(44.0)
  b.fromFloat(14.0)
  check sub(a, b).toInt() == 30
  check sub(a, b).toFloat() == 30.0

test "saturated substraction":
  var a = initQ16_16()
  var b = initQ16_16()
  a.fromInt(high(int16))
  b.fromInt(-1)
  check satSub(a, b).toInt() == high(int16)

  a.fromInt(low(int16))
  b.fromInt(1)
  check satSub(a, b).toInt() == low(int16)
