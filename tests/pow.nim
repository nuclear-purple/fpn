import unittest

import fpn

test "ipow":
  var a = initQ16_16()
  var res = initQ16_16()
  check a.ipow(0) == a.one()
  check a.ipow(1).rawData() == 0
  check a.ipow(2).rawData() == 0

  check a.one().ipow(0) == a.one()
  check a.one().ipow(1) == a.one()
  check a.one().ipow(2) == a.one()

  a.fromInt(3)

  check a.ipow(0) == a.one()
  check a.ipow(1) == a
  res.fromInt(9)
  check a.ipow(2) == res
