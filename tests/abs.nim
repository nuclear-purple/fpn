import unittest

import fpn

test "isSafeAbs":
  var q = initQ16_16()
  q.fromInt(low(int16))
  check not isSafeAbs(q)

  q.fromInt(high(int16))
  check isSafeAbs(q)

  q.fromInt(-5)
  check isSafeAbs(q)

  q.fromInt(5)
  check isSafeAbs(q)

test "ovAbs":
  var q = initQ16_16()
  q.fromInt(low(int16))

  check ovAbs(q).toInt() == high(int16)

  q.fromInt(high(int16))
  check ovAbs(q).toInt() == high(int16)

  q.fromInt(-5)
  check ovAbs(q).toInt() == 5

  q.fromInt(5)
  check ovAbs(q).toInt() == 5

test "satAbs":
  var q = initQ16_16()
  q.fromInt(low(int16))

  check satAbs(q).toInt() == low(int16)

  q.fromInt(high(int16))
  check satAbs(q).toInt() == high(int16)

  q.fromInt(-5)
  check satAbs(q).toInt() == 5

  q.fromInt(5)
  check satAbs(q).toInt() == 5

test "abs":
  var q = initQ16_16()
  q.fromInt(low(int16))

  expect OverflowError:
    try:
      abs(q)
    except:
      raise

  q.fromInt(high(int16))
  check abs(q).toInt() == high(int16)

  q.fromInt(-5)
  check abs(q).toInt() == 5

  q.fromInt(5)
  check abs(q).toInt() == 5

