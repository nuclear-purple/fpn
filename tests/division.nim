import unittest

import fpn

test "[over|under]flow_div":
  var a = initQ16_16()
  var b = initQ16_16()

  check not isOverflowDiv(a,b)
  check not isUnderflowDiv(a,b)
  check not isSafeDiv(a,b)

  a.fromInt(low(int16))
  b.fromInt(2)
  check not isOverflowDiv(a,b)
  check not isUnderflowDiv(a,b)
  check isSafeDiv(a,b)
  check toInt(a.ovDiv(b)) == low(int16) div 2

  a.fromInt(low(int16))
  b.fromInt(-1)
  check not isUnderflowDiv(a,b)
  check isOverflowDiv(a,b)
  check isSafeDiv(a,b) == false
  check toInt(a.ovDiv(b)) == high(int16)

test "division":
  var a = initQ16_16()
  var b = initQ16_16()
  b.fromInt(-1)

  check a.div(b).rawData() == 0
  check rawData(a / b) == 0
  a /= b
  check a.rawData() == 0

  b.fromInt(0)

  check ovDiv(a, b) == high(a)
  a.fromInt(1)
  check ovDiv(a, b) == high(a)
  a.fromInt(-1)
  echo b.isZero()
  echo ovDiv(a, b)
  echo ovDiv(a, b) == low(a)
#  expect OverflowError:
#    try:
#      a / b
#    except:
#      raise

test "saturated division":
  var a = initQ16_16()
  var b = initQ16_16()
  a.fromInt(low(int16))
  b.fromInt(-1)
  check satDiv(a, b).toInt() == high(int16)

