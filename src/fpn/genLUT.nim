# genLUT.nim
## Generates a lookup array for trigonometric functions.

import math, strformat
import fpn


proc genCos(first = 0.0, last = 2*PI, nSteps = 256) =
  ## Generates a lookup table and echoes it onto lut.nim
  let step = (last - first) / float64(nSteps)
  var x, cosx: float64
  var fpCos: fixedPoint64[32]
  var line = "  ["
  var lutFile = open("./lutCos.nim", mode = fmWrite)
  lutFile.writeLine("# cosine lookup table")
  lutFile.writeLine("")
  lutFile.writeLine(&"const cosLookUp: array[{nSteps + 1}, int64] =")
  for i in 0 .. nSteps:
    x = first + step * float64(i) # Number whose cosine will be calculated
    cosx = math.cos(x)
    fpCos.fromFloat(cosx)
    line.add(&"{fpCos.rawData():#011X}#[{cosx:.8f}]#, ")
    if i mod 16 == 15:
      lutFile.writeLine(line)
      line = "  "
  if line != "  " and line != "  [":
    lutFile.writeLine(line)
  lutFile.write("]\n")
  lutFile.close()

proc genArctan(first = -1.0, last = 1.0, nSteps = 256) =
  ## Generates an arctan lookup table and echoes it onto lut.nim
  let step = (last - first) / float64(nSteps)
  var x, arctanx: float64
  var fpArctan: fixedPoint64[32]
  var line = "  ["
  var lutFile = open("./lutArctan.nim", mode = fmWrite)
  lutFile.writeLine("# Arctangent lookup table")
  lutFile.writeLine("")
  lutFile.writeLine(&"const arctanLookUp: array[{nSteps + 1}, int64] =")
  for i in 0 .. nSteps: #Includes arctan(1)
    x = first + step * float64(i) # Number whose cosine will be calculated
    arctanx = math.arctan(x)
    fpArctan.fromFloat(arctanx)
    line.add(&"{fpArctan.rawData():#011X}#[{arctanx:.8f}]#, ")
    if i mod 16 == 15:
      lutFile.writeLine(line)
      line = "  "
  if line != "  " and line != "  [":
    lutFile.writeLine(line)
  lutFile.write("]\n")
  lutFile.close()

when isMainModule:
  genCos()
  genArctan()
