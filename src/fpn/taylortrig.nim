import fpn

proc fpsinz(z: FixedPoint): FixedPoint =
  ## Calculates and returns the sine of z = 2*x/pi (in radians).
  ## Utilizes the algorythm detailed in:
  ## https://www.nullhardware.com/blog/fixed-point-sine-and-cosine-for-embedded-systems/
  let Pi = z.pi()
  let a = 4 * (3 / Pi - fromSameType(z, 9) / 16)
  let b = 2 * a - fromSameType(z, 5) / 2
  let c = a - fromSameType(z, 3) / 2
  result = a * z - b * ipow(z, 3) + c * ipow(z, 5)

proc fpsinTaylor*(x: FixedPoint): FixedPoint =
  ## Calculates and returns the sine of x (in radians).
  ## Utilizes fpsinz as an intermediate proc.
  let Pi = x.pi()
  var z = x * 2 / Pi
  # Detect which quadrant the angle is in
  let quadrant = z.wholePart().toInt() mod 4
  z = z.floatPart()
  case quadrant:
  of 0:
    fpsinz(z)
  of 1:
    fpsinz(1-z)
  of 2:
    -fpsinz(z)
  of 3:
    -fpsinz(1-z)
  else:
    fromSameType(z)

proc fpcosTaylor*(x: FixedPoint): FixedPoint =
  ## Calculates and returns the cosine of x (in radians).
  ## Utilizes fpsinz as an intermediate proc
  let Pi = x.pi()
  # cos(x) = sin(x + pi/2)
  var z = x * 2 / Pi + 1
  # Detect which quadrant the angle is in
  let quadrant = z.wholePart().toInt() mod 4
  z = z.floatPart()
  case quadrant:
  of 0:
    fpsinz(z)
  of 1:
    fpsinz(1-z)
  of 2:
    -fpsinz(z)
  of 3:
    -fpsinz(1-z)
  else:
    fromSameType(z)
