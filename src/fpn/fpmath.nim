# fpmath.nim

## Defines trigonometric functions and mathematical utilities.
## For use with the fpn module (fixed point numbers).

import fpn
include lutCos, lutArctan

var Epsilon = 0b100001 # Margin of error for comparing fp numbers.

# Math operations

proc sign*(x: FixedPoint): int {.inline.} =
  result = (if x.isNeg: -1 else: 1)

proc sqrt*(x: FixedPoint) : typeof(x) =
  ## Returns the square root of x.
  ## Unlike isqrt, this one takes into account the fractional bits.

  let two = fromSameType(x, 2)
  let frac = x.rawFracBits
  let lowestBit = fromSameType(x, 1, frac)
  var currentBit = x.zero
  var test = x.zero
  var testSquared = test

  # Determine an initial estimate.
  # If x >= 1, then est = sqrt(2 ^ ilog2(x)) = 2 ^ (ilog2(x)/2)
  # But if x < 1, result = 0 and start counting down from the first frac bit
  if x >= x.one:
    result.setData(2 shl ((ilog2(x)/2).toInt + frac - 1))
    currentBit = result / two
  else:
    result = x.zero
    currentBit = x.one

  while currentBit >= lowestBit:
    if result * result == x:
      return result
    test = result + currentBit
    testSquared = test * test # Tests the square
    if testSquared <= x:
      result += currentBit
    currentBit /= two

#[ proc fpPow(x, y: FixedPoint): FixedPoint =
  discard

proc fpLog(x, base: FixedPoint): FixedPoint =
  discard

proc fpExp(x: FixedPoint): FixedPoint =
  discard ]#

proc lerp*(value1, value2, t: FixedPoint): FixedPoint =
  value1 + t * (value2 - value1)

# Trigonometric functions

proc fpcos*(x: FixedPoint): FixedPoint =
  ## Returns the cosine of x in radians, based on a lookup table.
  const lutLength = len(cosLookUp) - 1
  let tau = 2 * x.pi
  # cos(-x) == cos(x), so take the abs value.
  # also take x mod 2PI because the LUT is only defined until 2PI.
  # Next, set the index to the range [0, 256)
  let xIndex = ((abs(x) mod tau) / tau) * lutLength
  let i = xIndex.toInt
  let t = xIndex.floatPart
  var cosMin, cosMax: typeof(x)
  cosMin.setData(cosLookUp[i] shr (32 - x.rawFracBits))
  cosMax.setData(cosLookUp[i + 1] shr (32 - x.rawFracBits))
  return lerp(cosMin, cosMax, t)

proc fpsin*(x: FixedPoint): FixedPoint {.inline.} =
  ## Returns the sine of x in radians, based on a lookup table.
  fpcos(x - x.pi/2)

proc fptan*(x: FixedPoint): FixedPoint =
  ## Calculates and returns the tangent of x (in radians).
  fpsin(x)/fpcos(x)

proc fparctan*(x: FixedPoint): FixedPoint =
  ## Returns the arctangent of x in radians, based on a lookup table.
  let piHalf = x.pi / 2
  if x > x.one: return piHalf - fparctan(x.one / x)
  elif x < -x.one: return -piHalf - fparctan(x.one / x)
  elif x == x.one:
    result.setData(arctanLookUp[^1] shr (32 - x.rawFracBits))
    return result

  const lutLength = len(arctanLookUp) - 1
  var xIndex = (x + x.one) * lutLength / 2
  let i = xIndex.toInt
  let t = xIndex.floatPart
  var minValue, maxValue: typeof(x)
  minValue.setData(arctanLookUp[i] shr (32 - x.rawFracBits))
  maxValue.setData(arctanLookUp[i + 1] shr (32 - x.rawFracBits))
  return lerp(minValue, maxValue, t)

proc fparctan2*(x, y: FixedPoint): FixedPoint =
  if x.isEqualApprox(x.zero): result = x.sign * y.sign * x.pi / 2
  else: result = fparctan(y / x)

# Math utilities

proc isEqualApprox*(x, y: FixedPoint, margin = Epsilon): bool =
  abs(x - y).rawData() < margin

proc toRadian*(x: FixedPoint): FixedPoint =
  ## Returns the value of the angle x (in degrees) converted to radians.
  (x.pi / fromSameType(x, 180)) * x

proc toDegree*(x: FixedPoint): FixedPoint =
  ## Returns the value of the angle x (in radians) converted to degrees.
  (fromSameType(x, 180) / x.pi) * x

proc stepify*(value, step: FixedPoint): FixedPoint =
  if step != step.zero:
    floor(value / step + fromSameType(value, 1, 1)) * step
  else:
    value
