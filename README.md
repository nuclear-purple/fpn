# NIM Fixed Point Number library

fpn is a library for [fixed point numbers](https://en.wikipedia.org/wiki/Fixed-point_arithmetic) in Nim.

This is a modified version of [fpn](https://gitlab.com/lbartoletti/fpn) that I use in my deterministic physics library. It's optimized to be faster and more deterministic (reworked float conversion functions), at the cost of safety, as I had to remove several overflow checks to achieve better speed.

There is also a new math module with trigonometric and square root functions implemented via a [lookup table](https://en.wikipedia.org/wiki/Lookup_table).

You can create a new fpn type with the `genQM_N` proc. The default type is Q16_16, accessed by the `newFix` or `fpn` procs.

Keep in mind that using 64 bit integer types (Q32_32 and such) works, but they use much slower multiplication and division due to a lack of 128 bit integers in Nim.


## Examples

Add two numbers:

```
var a: FixedPoint32[16]
var b = newFix() # either way works

a.fromFloat(40.4)
b.fromFloat(1.6)
echo a + b
# 42
```

See tests for more examples.

## API Documentation

Documentation is available at https://nuclear-purple.gitlab.io/fpn/fpn.html . The documentation for the new features is incomplete, for now.

## TODO

- [X] Trigonometry functions (done)
- [ ] Find a way to use 128 bit integers to improve 64 bit math
- [ ] Improve documentation
