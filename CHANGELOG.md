# Changes from the main repo

This repo is forked from [lbartoletti/fpn](https://gitlab.com/lbartoletti/fpn)

## V1.02

+ Bug fixes:
	* ilog functions did not work due to a bug. That has been fixed.
+ Added more overloaded procs: using +=, -=, *= and /= between a FixedPoint and an int is now valid.
+ Added a fixed point math module (fpn/fpmath). It currently supports the following operations:
	* sine;
	* cosine;
	* tangent;
	* square root;
	* conversions from radians to degree and vice versa.

	* For more information on the algorithm used in the trigonometric functions, read [this](https://www.nullhardware.com/blog/fixed-point-sine-and-cosine-for-embedded-systems/) blog post.

### TODO:

+ Implement pow, exp and log functions;
+ Make tests for the new features added;
+ Improve documentation.

## v1.01

+ Added setter function for modifying fp.data: proc setData
+ Added functionality to the initQN_M declaration function
  * You can now declare a fpn object using var x = initQN_M(data, number of bits to left-shift)
  * It defaults to initQN_M(data=0, shift=n), so using initQ_M() should work normally
  * using var v = initQ_M(x: int) should equate to declaring
      var v = initQ_M()
      v.fromInt(x)
  * Added the same flexibility to proc fromSameType
+ high(a : int) and low(a: int) deprecated as of nim 1.4.
  * Therefore, switched all instances of high(a.data) with high(typeof(a.data))
  * Likewise, switched low(a.data) with low(typeof(a.data))
+ proc mul: changed expression result = ... (a.fracBits + b.facBits) - b.fracBits to just b.fracBits
+ Added operators for allowing FPNs to be multiplied directly by integers
+ Replaced some declarations that used floats with integer-to-fpn declarations. e.g. proc roundHalfUp, "pi" and "e"
